package hackerrank;

import java.util.HashMap;
import java.util.Map;

public class FurnitureOrder implements FurnitureOrderInterface {

    private HashMap<Furniture, Integer> furnitures;

    FurnitureOrder() {
        this.furnitures = new HashMap<Furniture, Integer>();
    }

    public void addToOrder(final Furniture type, final int furnitureCount) {
        int count = 0;
        if(furnitures.containsKey(type)) {
            count = furnitures.get(type);
        }
        furnitures.put(type, count + furnitureCount);
    }

    public HashMap<Furniture, Integer> getOrderedFurniture() {
        return new HashMap<Furniture, Integer>(furnitures);
    }

    public float getTotalOrderCost() {
        float cost = 0.0f;
        if (furnitures.isEmpty()) return cost;
        for (Map.Entry<Furniture, Integer> furniture : furnitures.entrySet()){
            cost += (furniture.getKey().cost() * furniture.getValue());
        }
        return cost;
    }

    public int getTypeCount(Furniture type) {
        if(furnitures.containsKey(type)) {
            return furnitures.get(type);
        }
        return 0;
    }

    public float getTypeCost(Furniture type) {
        if(furnitures.containsKey(type)) {
            return furnitures.get(type) * type.cost();
        }
        return 0.0f;
    }

    public int getTotalOrderQuantity() {
        if(!furnitures.isEmpty()) {
            return furnitures.values().stream()
                    .reduce(Integer::sum)
                    .get();
        }
        return 0;
    }
}